#
# detect_with_darwin_import
# - Generates .txt annotation files based on YOLO detections
# - Also imports .xml files to darwin
#


import os
import cv2
import torch
import torch.backends.cudnn as cudnn
import numpy as np
import shutil
import re

os.chdir(r"C:\Dropbox\Dropbox\CLOUD - SW\wur_whitefly_assay")
from models.common import DetectMultiBackend
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.testing_tools import *


from pathlib import Path
from darwin.importer import get_importer, ImportParser, import_annotations
from darwin.client import Client
from darwin.dataset import RemoteDataset




img_dir = r"C:\Research_data\whitefly_assay\1600x1600_batch3_images"
project_name = "whitefly_assay"
weights_filename = "weights/{}/batch2.pt".format(project_name)

#
# Make an output folder
#
img_output_dir = "output"
try:
    os.mkdir(img_output_dir)
except:
    pass
txt_output_dir = "output_labels_yolo"
try:
    os.mkdir(txt_output_dir)
except:
    pass
xml_output_dir = "output_labels_xml"
try:
    os.mkdir(xml_output_dir)
except:
    pass     



def yolo_to_voc(bboxes, class_number, class_names, width, height, filename, xml_output_dir):
    xml_filename = os.path.basename(filename).replace(".jpg", ".xml")
    xml_filename = os.path.join(xml_output_dir, xml_filename)

    # Start the XML file
    with open(xml_filename, 'w') as f:
      f.write('<annotation>\n')
      f.write('\t<folder>XML</folder>\n')
      f.write('\t<filename>' + os.path.basename(filename) + '</filename>\n')
      f.write('\t<path>' + filename + '</path>\n')
      f.write('\t<source>\n')
      f.write('\t\t<database>Unknown</database>\n')
      f.write('\t</source>\n')
      f.write('\t<size>\n')
      f.write('\t\t<width>' + str(width) + '</width>\n')
      f.write('\t\t<height>' + str(height) + '</height>\n')
      f.write('\t\t<depth>3</depth>\n') 
      f.write('\t</size>\n')
      f.write('\t<segmented>0</segmented>\n')
    
      for each_line in bboxes:
        # regex to find the numbers in each line of the text file
        yolo_array = each_line

        # initalize the variables
        class_number = 0
        x_yolo = 0.0
        y_yolo = 0.0
        yolo_width = 0.0
        yolo_height = 0.0

        # make sure the array has the correct number of items
        if len(yolo_array) == 5:
            # assign the variables
            object_name = class_names[class_number]
            x_yolo = float(yolo_array[1])
            y_yolo = float(yolo_array[2])
            yolo_width = float(yolo_array[3])
            yolo_height = float(yolo_array[4])

            # Convert Yolo Format to Pascal VOC format
            box_width = yolo_width * width
            box_height = yolo_height * height
            x_min = str(int(x_yolo * width - (box_width / 2)))
            y_min = str(int(y_yolo * height - (box_height / 2)))
            x_max = str(int(x_yolo * width + (box_width / 2)))
            y_max = str(int(y_yolo * height + (box_height / 2)))

            # write each object to the file
            f.write('\t<object>\n')
            f.write('\t\t<name>' + object_name + '</name>\n')
            f.write('\t\t<pose>Unspecified</pose>\n')
            f.write('\t\t<truncated>0</truncated>\n')
            f.write('\t\t<difficult>0</difficult>\n')
            f.write('\t\t<bndbox>\n')
            f.write('\t\t\t<xmin>' + x_min + '</xmin>\n')
            f.write('\t\t\t<ymin>' + y_min + '</ymin>\n')
            f.write('\t\t\t<xmax>' + x_max + '</xmax>\n')
            f.write('\t\t\t<ymax>' + y_max + '</ymax>\n')
            f.write('\t\t</bndbox>\n')
            f.write('\t</object>\n')

      # Close the annotation tag once all the objects have been written to the file
      f.write('</annotation>\n')
      f.close() # Close the file
    return(xml_filename)











image_size_network = (640, 640)
image_size_real = 1600
half = True
conf_thres = 0.1
iou_thres = 0.1
classes = 0

save_image = False       # save the image file
save_txt = True        # save txt file
save_conf = False       # save confidence
save_xml = True
darwin_import = True

class_names = ["egg"]







#
# normalization gain whwh
#
gn = torch.tensor([image_size_real, image_size_real, image_size_real, image_size_real])  

#
# Load model
#
device = select_device()
model = DetectMultiBackend(weights_filename, device=device)
model.model.half()

#
# Run inference
#
model.warmup(imgsz=(1, 3, *image_size_network), half=True)  # warmup






filenames = os.listdir(img_dir)
img_files = [img_dir + "\\" + f for f in filenames if ".jpg" in f]
xml_filenames = []

for filename in img_files:
    
    txt_filename = txt_output_dir + "/" + os.path.basename(filename).replace(".jpg", ".txt")
    print(filename)
    
    #
    # Read, resize and condition image
    #
    im0 = cv2.imread(filename)
    im = letterbox(im0, image_size_network)[0]
    im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
    im = np.ascontiguousarray(im)
    
    im_out = im0.copy()
    
    #
    # Prepare image for inference
    #
    im = torch.from_numpy(im).to(device)
    im = im.half() if half else im.float()  # uint8 to fp16/32
    im /= 255  # 0 - 255 to 0.0 - 1.0
    if len(im.shape) == 3:
        im = im[None]  
    
    #
    # Inference
    #
    pred = model(im)
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, max_det=10000)
    lines_yolo = []
    lines_voc = []
    
    # Process predictions
    for i, det in enumerate(pred):  # per image
        if len(det):
            # Rescale boxes from img_size to im0 size
            det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()
    
            # Print results
            for c in det[:, -1].unique():
                n = (det[:, -1] == c).sum()  # detections per class
                
    
            # Write results
            for *xyxy, conf, cls in reversed(det):
                xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                line = (cls, *xywh, conf) if save_conf else (cls, *xywh)  # label format
                line_voc = [float(l) for l in line]
                
                lines_yolo.append(line)
                lines_voc.append(line_voc)
            
            
                coords = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                coords = yolobbox2bbox(*coords, image_size_real)
           
                start_point = (int(coords[0]), int(coords[1])) # x y start
                end_point = (int(coords[2]), int(coords[3]))   # x y end
                
                x1 = (int((start_point[0] + end_point[0])/2)) - 64
                x2 = (int((start_point[0] + end_point[0])/2)) + 64
                y1 = (int((start_point[1] + end_point[1])/2)) - 64
                y2 = (int((start_point[1] + end_point[1])/2)) + 64
                
                im_out = cv2.rectangle(im_out, start_point, end_point, (0,0,255), thickness=2)
                
                
                cropped_img = im0[y1:y2, x1:x2]
                
                if cropped_img.shape[0] == 128 and cropped_img.shape[1] == 128:
                    cv2.imwrite("output_128x128/" + os.path.basename(filename), cropped_img)
                
                        
            if save_image:
                output_filename = img_output_dir + "/" + os.path.basename(filename)
                cv2.imwrite(output_filename, im_out)
          
                
    if save_txt:  # Write to file
        
        if len(lines_yolo):
            for line in lines_yolo:
                with open(txt_filename, 'a') as f:
                    f.write(('%g ' * len(line)).rstrip() % line + '\n')  
        else:
            with open(txt_filename, 'w') as f:
                pass
            
    if save_xml:
        xml_filename = yolo_to_voc(lines_voc, 0, class_names, image_size_real, image_size_real, filename, xml_output_dir)                   
        xml_filenames.append(xml_filename)




    
       
darwin_import = True
if darwin_import:
    
    API_KEY = "h37f02x.me9Vm2gXFmMSZWu430y-a42hVC_H4z5g"
    client = Client.from_api_key(API_KEY)
    
    dataset: RemoteDataset = client.get_remote_dataset("whitefly_assays")
    parser: ImportParser = get_importer("pascal_voc")
    
    # annotation_paths = [r"C:\Dropbox\Dropbox\CLOUD - SW\wur_whitefly_assay/17-2_1_1.xml"]
    import_annotations(dataset, 
                       parser, 
                       xml_filenames,
                       append=False,
                       class_prompt=False)