#
# detectTest
# - Generates .txt annotation files based on YOLO detections
#


import os
import cv2
import torch
import torch.backends.cudnn as cudnn
import numpy as np
import shutil
import re

from models.common import DetectMultiBackend
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.testing_tools import *


os.chdir(r"C:\Dropbox\Dropbox\CLOUD - SW\wur_whitefly_assay")

img_dir = r"C:\Research_data\whitefly_assay\1600x1600_batch2_images"
project_name = "whitefly_assay"
weights_filename = "weights/{}/1600x1600.pt".format(project_name)

#
# Make an output folder
#
img_output_dir = "output"
try:
    os.mkdir(img_output_dir)
except:
    pass
txt_output_dir = "output_labels_yolo"
try:
    os.mkdir(txt_output_dir)
except:
    pass
       




image_size_network = (640, 640)
image_size_real = 1600
half = True
conf_thres = 0.1
iou_thres = 0.1
classes = 0

save_image = False       # save the image file
save_txt = True        # save txt file
save_conf = False       # save confidence

class_names = ["egg"]







#
# normalization gain whwh
#
gn = torch.tensor([image_size_real, image_size_real, image_size_real, image_size_real])  

#
# Load model
#
device = select_device()
model = DetectMultiBackend(weights_filename, device=device)
model.model.half()

#
# Run inference
#
model.warmup(imgsz=(1, 3, *image_size_network), half=True)  # warmup






filenames = os.listdir(img_dir)
img_files = [img_dir + "\\" + f for f in filenames if ".jpg" in f]


for filename in img_files:
    
    filename = img_files[7]

    txt_filename = txt_output_dir + "/" + os.path.basename(filename).replace(".jpg", ".txt")
    print(txt_filename)
    
    #
    # Read, resize and condition image
    #
    im0 = cv2.imread(filename)
    im = letterbox(im0, image_size_network)[0]
    im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
    im = np.ascontiguousarray(im)
    
    #
    # Prepare image for inference
    #
    im = torch.from_numpy(im).to(device)
    im = im.half() if half else im.float()  # uint8 to fp16/32
    im /= 255  # 0 - 255 to 0.0 - 1.0
    if len(im.shape) == 3:
        im = im[None]  
    
    #
    # Inference
    #
    pred = model(im)
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, max_det=10000)
    lines_yolo = []
    
    # Process predictions
    for i, det in enumerate(pred):  # per image
        if len(det):
            # Rescale boxes from img_size to im0 size
            det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()
    
            # Print results
            for c in det[:, -1].unique():
                n = (det[:, -1] == c).sum()  # detections per class
                
    
            # Write results
            for *xyxy, conf, cls in reversed(det):
                xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                line = (cls, *xywh, conf) if save_conf else (cls, *xywh)  # label format
                line_yolo = [float(l) for l in line]
                lines_yolo.append(line_yolo)
                
                if save_image:
                    coords = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                    coords = yolobbox2bbox(*coords, image_size_real)
                    
               
                    start_point = (int(coords[0]), int(coords[1]))
                    end_point = (int(coords[2]), int(coords[3]))                    
                    
                    im0 = cv2.rectangle(im0, start_point, end_point, (0,0,255), thickness=2)
            
                    
                if save_txt:  # Write to file
                    with open(txt_filename, 'a') as f:
                        f.write(('%g ' * len(line)).rstrip() % line + '\n')
                        
            if save_image:
                output_filename = img_output_dir + "/" + os.path.basename(filename)
                cv2.imwrite(output_filename, im0)
                    

