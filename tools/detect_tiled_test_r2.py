import os
import cv2
import torch
import torch.backends.cudnn as cudnn
import numpy as np
import shutil
import math

from models.common import DetectMultiBackend
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.testing_tools import *

crop_size = 1600





os.chdir(r"C:\Dropbox\Dropbox\CLOUD - SW\wur_whitefly_assay")


img_dir = r"C:\Research_data\whitefly_assay\junk\_base_test"
project_name = "whitefly_assay"
weights_filename = "weights/{}/batch1.pt".format(project_name)

image_size_network = (640, 640)
image_size_real = 1600
half = True
conf_thres = 0.2
iou_thres = 0.2
classes = 0

save_image = True       # save the image file
save_txt = False        # save txt file
save_conf = False       # save confidence







#
# normalization gain whwh
#
gn = torch.tensor([image_size_real, image_size_real, image_size_real, image_size_real])  

#
# Load model
#
device = select_device()
model = DetectMultiBackend(weights_filename, device=device)
model.model.half()

#
# Run inference
#
model.warmup(imgsz=(1, 3, *image_size_network), half=True)  # warmup




#
# Make an output folder
#
img_output_dir = os.path.join("output_tiled", project_name)
try:
    os.mkdir(img_output_dir)
except:
    pass
    



files = os.listdir(img_dir)
files = [img_dir + "/" + f for f in files]
files = [f for f in files if ".jpg" in f]
for file in files:
    all_coords = []
    all_labels = []
    all_confs = []
    image = cv2.imread(file)
    
    #
    # Round down first
    #
    s = image.shape
    l = rounddown(s[0])
    w = rounddown(s[1])
    cropped_image = image[0:l, 0:w]
    
    #
    # Get cropped images
    #
    s = cropped_image.shape
    l = s[0]
    w = s[1]
    d1 = math.floor(l/crop_size)
    d2 = math.floor(w/crop_size)
    
    for x in range(0, d1):
        for y in range(0, d2):
            # print(os.path.basename(file), x, y)
            tiled_image = cropped_image[(x*crop_size):((x+1)*crop_size), 
                                        (y*crop_size):((y+1)*crop_size), ]
            
            
                       
            #
            # Read, resize and condition image
            #
            im0 = tiled_image
            im = letterbox(im0, image_size_network)[0]
            im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
            im = np.ascontiguousarray(im)
            
            #
            # Prepare image for inference
            #
            im = torch.from_numpy(im).to(device)
            im = im.half() if half else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  
            
            #
            # Inference
            #
            pred = model(im)
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes, max_det=10000)
            
            
            #
            # Process predictions
            #
            n = 0
            for i, det in enumerate(pred):  
                if len(det):
                    #
                    # Rescale boxes from img_size to im0 size
                    #
                    det[:, :4] = scale_coords(im.shape[2:], 
                                              det[:, :4], 
                                              im0.shape).round()
                    
                    #
                    # Get number of detections
                    #
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        
                    #
                    # Write results
                    #
                    for *xyxy, conf, cls in reversed(det):
                        coords = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                        coords = yolobbox2bbox(*coords, image_size_real)
                        coords = [int(c) for c in coords]
             
                        
                        new_coords = [0] * 4
                        new_coords[0] = coords[0] + (y * image_size_real)
                        new_coords[1] = coords[1] + (x * image_size_real)
                        new_coords[2] = coords[2] + (y * image_size_real)
                        new_coords[3] = coords[3] + (x * image_size_real)
                      
                        # print(coords, new_coords)
                        all_coords.append(new_coords)
                        all_labels.append(int(cls))
                        all_confs.append(float(conf))
                    

    
 
    
    print(os.path.basename(file), len(all_coords))




    
    
    # Write results
    for cc, c, conf in zip(all_coords, all_labels, all_confs):
        start_point = (int(cc[0]), int(cc[1]))
        end_point = (int(cc[2]), int(cc[3]))                    
        txt_point = [cc[0]-15, cc[1]-15]
        
        image = cv2.putText(image, 
                            str(round(float(conf), 2)), 
                            txt_point, 
                            cv2.FONT_HERSHEY_SIMPLEX, 
                            1, 
                            (0,0,255), 
                            2, 
                            cv2.LINE_AA)
        image = cv2.rectangle(image, start_point, end_point, (0,0,255), thickness=2)
        

    
    if save_image:
        image = cv2.resize(image, [5000, 5000])
        output_filename = img_output_dir + "/" + os.path.basename(file)
        cv2.imwrite(output_filename, image)




