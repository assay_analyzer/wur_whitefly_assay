import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

os.getcwd()
# filename = "220725-2.1.1_1_2.jpg"
# filename = "220725 3.2_1_4.jpg"
# filename = "220725 3.2_1_2.jpg"
# filename = "220725 3.2_1_1.jpg"
# filename = "220725 3.2_0_2.jpg"
# filename = "220725-24.2.2_3_1.jpg"
im0 = cv2.imread(r"output_128x128/{}".format(filename))
im0 = cv2.cvtColor(im0, cv2.COLOR_BGR2RGB)
# imx = cv2.cvtColor(im0, cv2.COLOR_BGR2HSV)
imx = cv2.cvtColor(im0, cv2.COLOR_BGR2Lab)


imx = cv2.medianBlur(imx, 5)

# kernel = np.array([[0, -1, 0],
#                    [-1, 5,-1],
#                    [0, -1, 0]])
# imx = cv2.filter2D(src=imx, ddepth=-1, kernel=kernel)


c1,c2,c3 = cv2.split(imx)

Z = c3.reshape((-1))
# convert to np.float32
Z = np.float32(Z)
# define criteria, number of clusters(K) and apply kmeans()
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 2, 1.0)
K = 2
ret,label,center=cv2.kmeans(Z,K,None,criteria,3,cv2.KMEANS_RANDOM_CENTERS)
# Now convert back into uint8, and make original image



# center = np.uint8(center)
# res = center[label.flatten()]
# res2 = res.reshape((imx.shape))


# get the most popular color and specify it is black
black_color = np.argmax(np.bincount(label.flatten()))

# generate binary map with white color
center = np.ones((K,1), dtype = 'uint8') * 255
center[black_color] = [0]
res = center[label.flatten()]
res2 = res.reshape((imx.shape[0], imx.shape[1], 1))


black = np.zeros((128, 128, 3), dtype = "uint8")
mask_black = np.stack((res2,)*3, axis=-1).reshape(128, 128, 3)
# black = cv2.bitwise_or(black, mask_black)
res2 = cv2.bitwise_and(im0, mask_black)

# res2 = cv2.medianBlur(res2, 5)

plt.subplot(1,2,1)
plt.imshow(im0)
plt.subplot(1,2,2)
plt.imshow(res2)
