
import os
import cv2
import math
import matplotlib.pyplot as plt
from PIL import Image





crop_size = 1600

base_dir = r"C:\Research_data\whitefly_assay"
image_dir = r"{}\_base_tomato".format(base_dir)
base_cropped_dir =r"{}\_base_tomato_cropped".format(base_dir)
train_cropped_dir = r"{}\{}x{}_batch3_images".format(base_dir, crop_size, crop_size)
os.chdir(base_dir)


os.mkdir(base_cropped_dir)
os.mkdir(train_cropped_dir)

def roundup(x):
    return int(math.ceil(x / 100.0)) * 100

def rounddown(x):
    return int(math.floor(x / 100.0)) * 100



#
# tiff to jpg converter
#

def tiff_to_jpeg(root, filename):
    tiff_filename = os.path.join(root, filename)
    file_ext = os.path.splitext(tiff_filename)[1].lower()
    if file_ext == ".tif":
        jpeg_filename = os.path.splitext(tiff_filename)[0] + ".jpg"
        if os.path.isfile(jpeg_filename):
            print(" jpeg file already exists for {}".format(filename))
        else:
            try:
                im = Image.open(os.path.join(root, filename))
                print("Generating jpeg for {}".format(filename))
                im.thumbnail(im.size)
                im.save(jpeg_filename, "JPEG", quality=100)
            except Exception as e:
                print(e)
    else:
        print(filename, "is not a .tif file")











for root, dirs, files in os.walk(image_dir, topdown=False):
    for filename in files:
        tiff_to_jpeg(root, filename)




files = os.listdir(image_dir)
files = [image_dir + "/" + f for f in files]
for file in files:
    image = cv2.imread(file)
    s = image.shape
    
    l = rounddown(s[0])
    w = rounddown(s[1])
    cropped_image = image[0:l, 0:w]
    
    
    cropped_file = base_cropped_dir + "/" + os.path.basename(file).replace(".jpg", "_cropped.jpg")
    print(s, l, w, cropped_image.shape)
    print(cropped_file)
    cv2.imwrite(cropped_file, cropped_image)
    
    
    
files = os.listdir(base_cropped_dir)
files = [base_cropped_dir + "/" + f for f in files]
for file in files:   
    
    image = cv2.imread(file)
    s = image.shape
    l = s[0]
    w = s[1]
    d1 = math.floor(l/crop_size)
    d2 = math.floor(w/crop_size)
    
    # print(l, w)
    # print(l/crop_size, w/crop_size)
    # print(d1, d2)
    
    print(d1 * d2)
    
    for x in range(0, d1):
        for y in range(0, d2):
            cropped_image = image[(x*crop_size):((x+1)*crop_size), (y*crop_size):((y+1)*crop_size), ]
            # do some debugging
            # plt.figure()
            # plt.imshow(cropped_image)
            
            file = os.path.basename(file)
            cropped_file = train_cropped_dir + "/" + os.path.basename(file).replace("_cropped.jpg", "_{}_{}.jpg".format(x, y))
            print(cropped_file, cropped_image.shape)
            cv2.imwrite(cropped_file, cropped_image)
            
    
    
    
    
    
    
    
    

