#
#
# Whitefly Assay Analyzer
# Authored by: DJAR
# Wageningen University and Research
# Greenhouse Technology Business Unit
# 2022/08/04
#
#

import os
import cv2
import torch
import torch.backends.cudnn as cudnn
import numpy as np
import shutil
import math
import time
import pandas as pd
from datetime import datetime

from models.common import DetectMultiBackend
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.testing_tools import *

from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 
from PyQt5.QtCore import * 
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap, QColor, QImage
from PyQt5 import QtWidgets, uic
import sys





#
# Set directories
#
UI_FILE = "gui.ui"


#
# Algorithm constants
#
project_name = "whitefly_assay"
weights_filename = "weights/{}/batch3.pt".format(project_name)
crop_size = 1600
image_size_network = (640, 640)
image_size_real = 1600
conf_thres = 0.2
iou_thres = 0.2
L_filter = 25
W_filter = 25
classes = 0


save_image = True       # save the image file
save_txt = False        # save txt file
save_conf = False       # save confidence


#
# normalization gain whwh
#
gn = torch.tensor([image_size_real, image_size_real, image_size_real, image_size_real])  

#
# Load model
#
# half = True
# device = select_device()
# model = DetectMultiBackend(weights_filename, device=device)
# model.model.half()

half = False
device = torch.device("cpu")
model = DetectMultiBackend(weights_filename, device=device)
model.model.float()


#
# Run inference
#
model.warmup(imgsz=(1, 3, *image_size_network), half=half)  # warmup

#
# Make output folders
#
img_output_dir = "output"
try:
    os.mkdir(img_output_dir)
except:
    pass
xlsx_output_dir = "output_xlsx"
try:
    os.mkdir(xlsx_output_dir)
except:
    pass







class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi(UI_FILE, self) # Load the .ui file

        self.setWindowFlag(Qt.FramelessWindowHint)
        
        self.logText("[SYSTEM] Welcome to Whitefly Assay Analyzer v1.0")
      

        # self.startButton.setIconSize(QSize(50, 50))
        # self.stopButton.setIconSize(QSize(50, 50))
        self.titleLabel.setText("   Whitefly Assay Analyzer v1.0")
         
        # 
        # Just set logos
        #
        self.setWindowIcon(QIcon("icons/wur.png"))
        self.pixmap2 = QPixmap('icons/wur_logo.png')    
        self.pixmap2 = self.pixmap2.scaled(150+100, 100+100, Qt.KeepAspectRatio)
        self.wurLogoLabel.setPixmap(self.pixmap2)

        #
        # Set event for imageBox hover
        #
        self.imageBox.installEventFilter(self)
        self.imageBox.setMouseTracking(1)
        self.output_image = None


        #
        # Button functions
        #
        self.closeButton.clicked.connect(self.closeApplication)
        self.openSingleImageButton.clicked.connect(self.openImage)
        self.openMultipleImageButton.clicked.connect(self.openMultipleImages)

        self.show() # Show the GUI
    

    #
    # Event functions
    #
    def map_values(self, x, in_min, in_max, out_min, out_max):
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

    def mousePressEvent(self, event):
        self.offset = event.pos()

    def mouseMoveEvent(self, event):
        x=event.globalX()
        y=event.globalY()
        x_w = self.offset.x()
        y_w = self.offset.y()
        self.move(x-x_w, y-y_w)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.MouseMove:
            x = event.x() 
            y = event.y() 
            try:
                if self.output_image is not None:
                    hei, wid, _ = self.output_image.shape
                    mapped_x = self.map_values(x, 0, 700, 0, wid)
                    mapped_y = self.map_values(y, 0, 700, 0, hei)

        
                    roi = 150
                    mapped_x = int(mapped_x)
                    mapped_y = int(mapped_y)
                    crop_img = self.output_image[max(0, mapped_y - roi): min(mapped_y + roi, hei), 
                                                max(0, mapped_x - roi): min(mapped_x + roi, wid)]
                    cbox = self.findChild(QtWidgets.QLabel, "zoomBox")
                    self.displayImage(crop_img, cbox, True)
            except Exception as e:
                print(e)
            return True
        return False

    def closeApplication(self):
        app.quit()
        
    def displayImage(self, img, cam_box, scale):
        rgb_image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        
        dw = cam_box.frameGeometry().width()
        dh = cam_box.frameGeometry().height()
        
        p = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
        if scale == True:
            p = p.scaled(dw, dh, Qt.KeepAspectRatio)
        else:
            cam_box.setScaledContents(True)
        
        cam_box.setPixmap(QPixmap.fromImage(p))

    def logText(self, text):
        self.consoleTextEdit.insertPlainText(text + "\n")
    def updateImage(self, img):
        self.output_image = img
    def updateFilename(self, fname):
        self.filenameLabel.setText(fname)
    def updateCount(self, count):
        self.eggsLabel.setText(str(count))
    def updateTime(self, elapsed_time):
        self.timeElapsedLabel.setText(str(elapsed_time)  + " s")
    def toggleButtons(self, toggle):
        # disable
        if toggle == 0:   
            self.saveCheckBox.setEnabled(False)
            self.thresholdBox.setEnabled(False)
            self.openSingleImageButton.setEnabled(False)
            self.openMultipleImageButton.setEnabled(False)
            self.openSingleImageButton.setStyleSheet("color: '#cccccc'; background-color: '#ffffff'; border: none;")
            self.openMultipleImageButton.setStyleSheet("color: '#cccccc'; background-color: '#ffffff'; border: none;")
        # enable
        if toggle == 1:
            self.saveCheckBox.setEnabled(True)
            self.thresholdBox.setEnabled(True)
            self.openSingleImageButton.setEnabled(True)
            self.openMultipleImageButton.setEnabled(True)
            self.openSingleImageButton.setStyleSheet("color: '#000000'; background-color: '#eeeeee'; border: none;")
            self.openMultipleImageButton.setStyleSheet("color: '#000000'; background-color: '#eeeeee'; border: none;")


    def openImage(self):
        fname = QFileDialog.getOpenFileName(self, 'Open image', "", "Image files (*.tif *.jpg *.png)")[0]
        fname = str(fname)
        CONF_THRESHOLD = self.thresholdBox.value()
        SAVE_IMAGE_OUTPUT = self.saveCheckBox.isChecked()
        cbox = self.findChild(QtWidgets.QLabel, "imageBox")

        if fname != "":
            self.toggleButtons(0)
            pool = QThreadPool.globalInstance()
            runnable = imageProcessor([fname], 
                                    CONF_THRESHOLD, 
                                    SAVE_IMAGE_OUTPUT,
                                    cbox,
                                    self.displayImage,
                                    self.logText,
                                    self.updateImage,
                                    self.updateCount,
                                    self.updateFilename,
                                    self.updateTime,
                                    self.toggleButtons)
            pool.start(runnable)
       
    def openMultipleImages(self):
        fnames = QFileDialog.getOpenFileNames(self, 'Open image', "", "Image files (*.jpg *.tif *.png)")[0]
        CONF_THRESHOLD = self.thresholdBox.value()
        SAVE_IMAGE_OUTPUT = self.saveCheckBox.isChecked()
        cbox = self.findChild(QtWidgets.QLabel, "imageBox")

        
        if len(fnames):
            self.toggleButtons(0)
            pool = QThreadPool.globalInstance()
            runnable = imageProcessor(fnames, 
                                    CONF_THRESHOLD, 
                                    SAVE_IMAGE_OUTPUT,
                                    cbox,
                                    self.displayImage,
                                    self.logText,
                                    self.updateImage,
                                    self.updateCount,
                                    self.updateFilename,
                                    self.updateTime,
                                    self.toggleButtons)
            pool.start(runnable)


            
class imageProcessor(QRunnable):
    def __init__(self, fnames, threshold, save_image, cbox, displayImage, logText, updateImage, updateCount, updateFilename, updateTime, toggleButtons):
        super().__init__()
        self.fnames = fnames
        self.cbox = cbox
        self.threshold = threshold
        self.save_image = save_image
        self.displayImage = displayImage
        self.updateImage = updateImage
        self.updateCount = updateCount
        self.updateTime = updateTime
        self.updateFilename = updateFilename
        self.logText = logText
        self.toggleButtons = toggleButtons
        

    @pyqtSlot()
    def run(self):
        CONF_THRESHOLD = self.threshold
        cbox = self.cbox

        columns = ["Index", "DateTime", "Filename" ,"Count", "ElapsedTime", "Threshold"]
        xlsx_df = pd.DataFrame(columns=columns)

        for f_index, fname in enumerate(self.fnames):
            start = time.time()
            now = datetime.now()
            dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
            dt_string2 = now.strftime("%Y_%m_%d %H_%M_%S")
            fname = str(fname)
            print(fname)
            
            if fname != "":
                all_coords = []
                all_labels = []
                all_confs = []
                image = cv2.imread(fname)
                
                #
                # Round down first
                #
                s = image.shape
                l = rounddown(s[0])
                w = rounddown(s[1])
                cropped_image = image[0:l, 0:w]
                
                #
                # Get cropped images
                #
                s = cropped_image.shape
                l = s[0]
                w = s[1]
                d1 = math.floor(l/crop_size)
                d2 = math.floor(w/crop_size)
                
                for x in range(0, d1):
                    for y in range(0, d2):
                        # print(os.path.basename(file), x, y)
                        tiled_image = cropped_image[(x*crop_size):((x+1)*crop_size), 
                                                    (y*crop_size):((y+1)*crop_size), ]
                        
                        
                                
                        #
                        # Read, resize and condition image
                        #
                        im0 = tiled_image
                        im = letterbox(im0, image_size_network)[0]
                        im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
                        im = np.ascontiguousarray(im)
                        
                        #
                        # Prepare image for inference
                        #
                        im = torch.from_numpy(im).to(device)
                        im = im.half() if half else im.float()  # uint8 to fp16/32
                        im /= 255  # 0 - 255 to 0.0 - 1.0
                        if len(im.shape) == 3:
                            im = im[None]  
                        
                        #
                        # Inference
                        #
                        pred = model(im)
                        pred = non_max_suppression(pred, conf_thres, iou_thres, classes, max_det=10000)
                        
                        
                        #
                        # Process predictions
                        #
                        n = 0
                        for i, det in enumerate(pred):  
                            if len(det):
                                #
                                # Rescale boxes from img_size to im0 size
                                #
                                det[:, :4] = scale_coords(im.shape[2:], 
                                                        det[:, :4], 
                                                        im0.shape).round()
                                
                                #
                                # Get number of detections
                                #
                                for c in det[:, -1].unique():
                                    n = (det[:, -1] == c).sum()  # detections per class
                                    
                                #
                                # Write results
                                #
                                for *xyxy, conf, cl in reversed(det):
                                    if conf >= CONF_THRESHOLD:
                                        coords = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                                        coords = yolobbox2bbox(*coords, image_size_real)
                                        coords = [int(c) for c in coords]
                            
                                        
                                        new_coords = [0] * 4
                                        new_coords[0] = coords[0] + (y * image_size_real)
                                        new_coords[1] = coords[1] + (x * image_size_real)
                                        new_coords[2] = coords[2] + (y * image_size_real)
                                        new_coords[3] = coords[3] + (x * image_size_real)

                                        L = abs(new_coords[0]  - new_coords[2])
                                        W = abs(new_coords[1]  - new_coords[3])
                                        if L >= L_filter and W >= W_filter:
                                            all_coords.append(new_coords)
                                            all_labels.append(int(cl))
                                            all_confs.append(float(conf))

                                            start_point = (int(new_coords[0]), int(new_coords[1]))
                                            end_point = (int(new_coords[2]), int(new_coords[3]))                    
                                            txt_point = [new_coords[0]-15, new_coords[1]-15]
                                            
                                            image = cv2.putText(image, 
                                                                str(round(float(conf), 2)), 
                                                                txt_point, 
                                                                cv2.FONT_HERSHEY_SIMPLEX, 
                                                                1, 
                                                                (0,0,255), 
                                                                2, 
                                                                cv2.LINE_AA)
                                            image = cv2.rectangle(image, start_point, end_point, (0,0,255), thickness=2)     

                                            all_coords.append(new_coords)
                                            all_labels.append(int(cl))
                                            all_confs.append(float(conf))
                    
                
                eggs_count = len(all_coords)
                end = time.time()
                elapsed_time = round((end-start), 3)
                xlsx_df = xlsx_df.append({"Index": f_index+1, "DateTime": dt_string, "Filename": os.path.basename(fname),
                                        "Count": eggs_count, "ElapsedTime": elapsed_time, "Threshold": CONF_THRESHOLD}, ignore_index=True)
                
                img_filename = img_output_dir + "/" + os.path.basename(fname)
                if self.save_image:
                    cv2.imwrite(img_filename, image)
                    self.logText("[SYSTEM] Output image file saved in: {}".format(img_filename))

                self.updateImage(image)
                self.displayImage(image, cbox, False)
                self.updateCount(eggs_count)
                self.updateTime(elapsed_time)
                self.updateFilename(os.path.basename(fname))
                self.logText("[SYSTEM] {}/{} {} Count: {} Time elapsed: {}".format(f_index+1, 
                                                                                    len(self.fnames), 
                                                                                    fname, 
                                                                                    eggs_count, 
                                                                                    elapsed_time))
        xlsx_filename = xlsx_output_dir + "/" + dt_string2 + ".xlsx"
        xlsx_df.to_excel(xlsx_filename, index=False)
        self.logText("[SYSTEM] Excel file saved in: {}".format(xlsx_filename))
        self.toggleButtons(1)
            



        
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
    window = Ui() # Create an instance of our class
    sys.exit(app.exec())# Start the application
