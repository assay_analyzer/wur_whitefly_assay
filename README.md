# wur_whitefly_assay

### 1. Download the package:

<p>https://www.dropbox.com/s/naqc9xkn0cin837/wur_whitefly_assay_installer.zip?dl=0</p>

### 2. Run the following:

<p>vcxsrv-64.1.20.14.0.installer.exe <br>
- After installation, run XLaunch then enable "Disable access control" <br>
  Docker Desktop Installer.exe <br>
- After installation, open Docker Desktop <br></p>

### 3. Run the GUI using run_gui.bat

## To initialize:

```
cd existing_repo
git remote add origin https://assay_analyzer:glpat-zAnycjjsD3vbGExPzynv@git.wur.nl/assay_analyzer/wur_whitefly_assay.git
git branch -M main
git push -uf origin main https://assay_analyzer:glpat-zAnycjjsD3vbGExPzynv@git.wur.nl/assay_analyzer/wur_whitefly_assay.git
```

## To clone:

```
git clone https://assay_analyzer:glpat-zAnycjjsD3vbGExPzynv@git.wur.nl/assay_analyzer/wur_whitefly_assay.git
```

## Setting up the Docker container

```
docker pull ultralytics/yolov5:latest
docker exec -it ultralytics/yolov5:latest /bin/bash
sudo apt-get install python3-pyqt5
sudo apt-get install pyqt5-dev-tools
sudo apt-get install qttools5-dev-tools
sudo apt-get install '^libxcb.\*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev
```

## To update Docker image

```docker commit <CONTAINER_NAME> coredanblue/assay_analyzer:latest
docker image ls # look for the <IMAGE_ID>
docker tag <IMAGE_ID> coredanblue/assay_analyzer:latest
docker push coredanblue/assay_analyzer:latest
```

## Docker on WSL2

```sudo apt update
sudo apt -y install docker.io
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
curl -s -L https://nvidia.github.io/libnvidia-container/experimental/$distribution/libnvidia-container-experimental.list | sudo tee /etc/apt/sources.list.d/libnvidia-container-experimental.list
```
